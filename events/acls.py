# from .keys import PEXELS_API_KEY
# from .keys import OPEN_WEATHER_API_KEY as weather_key
import requests
import json

def get_photo(city, state):
    # Get information from this URL
    response = requests.get(f"https://api.pexels.com/v1/search?query={city}{state}", headers={"Authorization": "QEDELBUOXiIWk9okf7M5Lc1Asy1QwHVFM0qvVtmMtv1MXTVStdzx66eC"})
    content = json.loads(response.content)
    try:
        url = {"picture_url": content["photos"][0]["src"]["original"]}
        print(url)
        return url
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather(city, state):
    ...
#     # Get coords, parse coords, plug in coords into the other API url, return API url to view function
#     get_coords = requests.get(f"http://api.openweathermap.org/geo/1.0/direct?q={city name},{state code},{country code}&limit={limit}&appid={API key}")
#     content = json.loads(get_coords.content)
#    response = requests.get("https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid=", weather_key)
# Need US country code in your URL or spell out the state?
